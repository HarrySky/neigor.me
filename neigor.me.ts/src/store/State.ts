import { ScreenData } from 'DataTypes';
import { getScreenData } from './utils';

interface ScreenState {
  screen: ScreenData;
}

interface AboutState {
  aboutOpen: boolean;
}

export interface AppState
  extends ScreenState,
    AboutState {}

export const INITIAL_STATE: AppState = {
  screen: getScreenData(),
  aboutOpen: false,
};
