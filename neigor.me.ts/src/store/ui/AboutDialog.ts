import { Dispatch } from 'redux';
import { aboutClose } from 'store/actions/AboutDialog';
import { AppState } from 'store/State';

interface State {
  isOpen: boolean;
  isMobile: boolean;
}

interface Actions {
  close(): void;
}

export interface Props extends State, Actions {}

export function mapStateToProps(state: AppState): State {
  return {
    isOpen: state.aboutOpen,
    isMobile: state.screen.isMobile,
  };
}

export function mapDispatchToProps(dispatch: Dispatch): Actions {
  return {
    close: () => dispatch(aboutClose),
  };
}
