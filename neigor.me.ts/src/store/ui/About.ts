import { Dispatch } from 'redux';
import { aboutOpen } from 'store/actions/About';

export interface Props {
  open(): void;
}

export function mapDispatchToProps(dispatch: Dispatch): Props {
  return {
    open: () => dispatch(aboutOpen),
  };
}
