import { Actions, AboutAction } from 'store/actions';

export const aboutOpen: AboutAction = {
  type: Actions.ABOUT_OPEN,
};
