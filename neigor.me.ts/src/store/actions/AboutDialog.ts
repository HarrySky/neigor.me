import { Actions, AboutAction } from 'store/actions';

export const aboutClose: AboutAction = {
  type: Actions.ABOUT_CLOSE,
};
