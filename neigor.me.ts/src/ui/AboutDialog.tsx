import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as React from 'react';
import 'ui/css/App.css';
import { connect } from 'react-redux';
import {
  Props,
  mapStateToProps,
  mapDispatchToProps
} from 'store/ui/AboutDialog';

export class AboutDialog extends React.PureComponent<Props> {
  render() {
    const age = new Date(
      Date.now() - new Date(1997, 5 - 1, 6).getTime()
    ).getFullYear() - 1970;

    return (
      <Dialog
        open={this.props.isOpen}
        onClose={this.props.close}
        aria-labelledby="about-me-title"
        fullScreen={this.props.isMobile}
      >
        <DialogTitle id="about-me-title">About Me</DialogTitle>
        <DialogContent>
          <DialogContentText component="div">
            I am{' '}
            <b title="Igor Nehorošev / Игорь Нехорошев">Igor Nehoroshev</b>,
            living in <b>Tallinn, Estonia</b> and making world a better{' '}
            <i>(and funnier)</i> place.
            
            <br />
            <br />

            <b>Age:</b> {age}
            <br />
            <b>Hometown:</b> Haapsalu, Estonia
            <br />
            <b>Alignment:</b>{' '}
            <a
              href="http://easydamus.com/chaoticgood.html"
              target="_blank"
              rel="noopener noreferrer"
            >
              Chaotic Good
            </a>

            <br />
            <br />

            <b>What I Do?</b>
            <ul>
              <li>
                Business IT <b>Bachelor at TalTech</b>
              </li>
              <li>
                <b>Software Developer</b>
                {' and '}
                <b>DevOps Engineer</b>
                {' at '}
                <a
                  href="https://martem.ee"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Martem AS
                </a>
              </li>
              <li>
                <b>Founder</b> of HashDivision OÜ
              </li>
              <li>
                <b>Contributor to Open-Source</b> via:
                <br />
                <u>GitLab</u> - Personal Projects
                <br />
                <u>GitHub</u> - Helping with PRs and Issues
              </li>
            </ul>
            <br />
            <small>
              <i>expected more info? ask or check my social networks ;)</i>
            </small>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.close} color="primary">
            The More I Know!
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

// tslint:disable-next-line: no-default-export
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutDialog);
