import Button from '@material-ui/core/Button';
import ExploreIcon from '@material-ui/icons/Explore';
import * as React from 'react';
import 'ui/css/App.css';

export class Mission extends React.PureComponent {
  render() {
    return (
      <Button
        variant="outlined"
        color="primary"
        fullWidth={true}
        target="_blank"
        href="https://neigor.me/mission"
        rel="noopener noreferrer"
      >
        <ExploreIcon className="left-icon" />
        Mission
      </Button>
    );
  }
}
